module CP1.Types
  ( CP1
  , hom
  , unhom
  , view
  , unview
  , T
  , projective
  , applyT
  , module Data.Complex
  ) where

import Data.Complex

data CP1 = CP1 (Complex Double) (Complex Double)

instance Eq CP1 where
  CP1 x1 y1 == CP1 x2 y2 = x1*y2 == x2*y1

hom :: Complex Double -> CP1
hom x = CP1 x 1

unhom :: CP1 -> Maybe (Complex Double)
unhom (CP1 x y) =
  if y == 0 then Nothing else Just (x/y)

view :: CP1 -> (Complex Double, Complex Double)
view (CP1 x y) = (x,y)

unview :: (Complex Double, Complex Double) -> Maybe CP1
unview (0,0) = Nothing
unview (x,y) = Just (CP1 x y)

data T = T (Complex Double) (Complex Double) (Complex Double) (Complex Double)

projective
  :: Complex Double
  -> Complex Double
  -> Complex Double
  -> Complex Double
  -> Maybe T
projective a b c d
  | a*d - b*c == 0 = Nothing
  | otherwise      = Just (T a b c d)

applyT
  :: T
  -> CP1
  -> CP1
applyT (T a b c d) (CP1 x y) = CP1 (a*x + b*y) (c*x + d*y)
