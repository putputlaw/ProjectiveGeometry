module Fields.Rational.Trigonometry
( circ
, epsApprox, angleToParam
, eps
) where

import Data.Ratio


circ :: Rational -> (Rational, Rational)
circ t = (rcos t, rsin t)

rtan, rcos, rsin :: Rational -> Rational
rtan t = 2*t     / (1-t*t)
rcos t = (1-t*t) / (1+t*t)
rsin t = 2*t     / (1+t*t)


epsApprox :: Double -> Rational
epsApprox = flip approxRational eps
  where eps = 1e-16

eps :: Double
eps = 1e-14

angleToParam :: Double -> Rational
angleToParam a = epsApprox $ tan (a/2)
