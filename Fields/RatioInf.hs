{- | Fields.RatioInf

The field of rationals extended with an infinity element

Warning: not every operation is defined / meaningful / law-obeying
  
  I try to stick to the rules of Normal Wildbergers extended rational numbers

-}

{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}

module Fields.RatioInf where

import Data.Ratio


data RatioInf a = Inf | Rat (Ratio a)
type RationalInf = RatioInf Integer

star_exn = error "* produced"

instance (Integral a, Show a) => Show (RatioInf a) where
  show (Rat q) = show q
  show Inf     = "∞"

instance Eq a => Eq (RatioInf a) where
  Inf   == Inf   = True
  Rat x == Rat y = x == y
  _ == _ = False

instance Integral a => Num (RatioInf a) where
  Rat x + Rat y = Rat (x+y)
  Inf   + Inf   = star_exn
  Inf   + _     = Inf

  Rat x * Rat y = Rat (x*y)
  Inf * Inf     = Inf
  Inf * Rat x   = if x == 0 then star_exn else Inf
  Rat x * Inf   = if x == 0 then star_exn else Inf

  abs (Rat x) = Rat (abs x)
  abs Inf     = Inf

  negate (Rat x) = Rat (negate x)
  negate Inf     = Inf
  
  signum (Rat x) = Rat $ signum x
  signum Inf     = error $ "signum of " ++ show Inf ++ " is undefined"

  fromInteger = Rat . fromInteger

instance Integral a => Fractional (RatioInf a) where
  Rat x / Rat y =
    case (x==0, y==0) of
         (_, False)    -> Rat (x/y)
         (False, True) -> Inf
         (True, True)  -> star_exn
  Inf / Inf     = star_exn
  Inf / Rat x   = Inf
  Rat x / Inf   = 0

  recip (Rat x) = if x == 0 then Inf else Rat (recip x)
  recip Inf     = 0

  fromRational = Rat . fromRational
