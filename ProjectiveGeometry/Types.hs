{-# LANGUAGE TypeFamilies #-}
module ProjectiveGeometry.Types where


class Projective t where
  data Point t :: *
  data Line  t :: *
  on :: Point t -> Line t -> Bool
  join :: Point t -> Point t -> Line t
  meet :: Line t -> Line t -> Point t


(><) :: Projective t => Line t -> Line t -> Point t
(><) = meet

(/:) :: Projective t => Point t -> Point t -> Line t
(/:) = join
