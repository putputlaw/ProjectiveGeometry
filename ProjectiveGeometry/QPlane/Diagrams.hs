{-# LANGUAGE TypeFamilies #-}
module ProjectiveGeometry.QPlane.Diagrams where

import ProjectiveGeometry.QPlane
import qualified Diagrams.Prelude as D


renderP :: (D.PathLike p, D.V p ~ D.R2, D.Transformable p, D.HasStyle p) => [P] -> p
renderP = D.mconcat . map toPoint
  where
  toPoint :: (D.PathLike p, D.V p ~ D.R2, D.Transformable p, D.HasStyle p) => P -> p
  toPoint p =
    case dehom p of
         Nothing -> D.mempty
         Just (x,y) -> D.circle 5 D.# D.translateX (fromRational x * 99)
                                  D.# D.translateY (fromRational y * 99)
                                  D.# D.fc D.black

