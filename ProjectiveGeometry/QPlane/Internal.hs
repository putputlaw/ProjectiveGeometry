{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE FlexibleInstances #-}
module ProjectiveGeometry.QPlane.Internal where

import Data.Ratio
import ProjectiveGeometry.Types

-------------------------------------------------------

type Vec2D = (Rational, Rational)
type Vec3D = (Rational, Rational, Rational)

zero3 :: Vec3D
zero3 = (0,0,0)

det2 :: (Vec2D, Vec2D) -> Rational
det2 ((a,b),(c,d)) = a*d - b*c

cross :: Vec3D -> Vec3D -> Vec3D
cross (a,b,c) (d,e,f) =
  ( det2 ((b,c),(e,f))
  , det2 ((c,a),(f,d))
  , det2 ((a,b),(d,e))
  )

dot3 :: Vec3D -> Vec3D -> Rational
dot3 (a,b,c) (d,e,f) = a*d + b*e + c*f

collinear3d :: Vec3D -> Vec3D -> Bool
collinear3d vec1 vec2 = cross vec1 vec2 == zero3



-------------------------------------------------------
data QPlane

type L = Line QPlane
type P = Point QPlane

instance Projective QPlane where
  data Point QPlane = P Vec3D
  data Line  QPlane = L Vec3D
  on (P x) (L g) = dot3 x g == 0
  join (P x) (P y) = L (cross x y)
  meet (L h) (L g) = P (cross g h)

-------------------------------------------------------

instance Show (Point QPlane) where
  show (P (x,y,z)) =
    case z of
         0 -> "(" ++ show (fromRational x :: Double) ++ ","
              ++ show (fromRational y :: Double) ++ ")*"
         1 -> "(" ++ show (fromRational x :: Double) ++ ","
              ++ show (fromRational y :: Double) ++ ")"
         _ -> show (normaliseP (P(x,y,z)))

instance Eq (Point QPlane) where
  P a == P b = collinear3d a b

hom :: Rational -> Rational -> P
hom x y = P (x,y,1)

dehom :: P -> Maybe Vec2D
dehom (P (x,y,z)) =
  if z == 0
     then Nothing
     else Just (x/z, y/z)

far :: P -> P
far (P (x,y,_)) = P (x,y,0)

close :: P -> P
close (P (x,y,0)) = P (x,y,1)
close p           = p

farLine :: L
farLine = far (hom 1 0) /: far (hom 0 1)

origin = hom 0 0

instance Show (Line QPlane) where
  show (L (x,y,z)) = if x == 0 && y == 0
    then show (fromRational x :: Double, fromRational y :: Double) ++ "*"
    else show (fromRational x :: Double, fromRational y :: Double, fromRational z :: Double)

instance Eq (Line QPlane) where
  L a == L b = collinear3d a b

normaliseL :: L -> L
normaliseL (L (x,y,z)) =
  if z /= 0
     then L (x/z, y/z, 1)
     else L (x,y,0)

normaliseP :: P -> P
normaliseP (P (x,y,z)) =
  if z /= 0
     then P (x/z, y/z, 1)
     else P (x,y,0)

lx, ly, lz :: L -> Rational
lx (L (x,_,_)) = x
lz (L (_,y,_)) = y
ly (L (_,_,z)) = z

px, py, pz :: P -> Rational
px (P (x,_,_)) = x
pz (P (_,y,_)) = y
py (P (_,_,z)) = z

invalidP :: P -> Bool
invalidP (P vec) = vec == zero3

invalidL :: L -> Bool
invalidL (L vec) = vec == zero3

xAxis, yAxis :: L
xAxis = origin /: hom 1 0
yAxis = origin /: hom 0 1
