{-# LANGUAGE TypeFamilies, EmptyDataDecls #-}
module ProjectiveGeometry.Fano where

import ProjectiveGeometry.Types

data Fano


instance Projective Fano where
  data Point Fano = A | B | C | D | E | F | G
    deriving (Eq, Show)
  data Line Fano  = ABD | ACF | DEF | BCE | AGE | BGF | DGC
    deriving (Eq, Show)

  on p l = elem (head $ show p) (show l)
  
  join x y =
   let cx:_ = show x
       cy:_ = show y
   in head $ filter
       (\line -> let set = show line in elem cx set && elem cy set)
       [ABD, ACF, DEF, BCE, AGE, BGF, DGC]

  meet g h =
   let set1 = show g
       set2 = show h
       both = filter (flip elem set2) set1
   in  head $ filter (\p -> show p == both) [A,B,C,D,E,F,G]
