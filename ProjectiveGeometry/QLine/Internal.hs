module QLine.Internal where

import Fields.RatioInf
import Data.Ratio

type P = (Rational, Rational)
type QInf = RationalInf

hom :: QInf -> P
hom (Rat x) = (x, 1)
hom Inf     = (1, 0)

dehom :: P -> QInf
dehom (x,y) = Rat x / Rat y

normalise :: P -> P
normalise (x,y) =
  let a = fromIntegral $ lcm (denominator x `max` 1) (denominator y `max` 1)
  in (a*x, a*y)
