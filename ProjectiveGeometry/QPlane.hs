module ProjectiveGeometry.QPlane
(
  -- * Type Class
  Projective (..)
, (><), (/:)
  -- * Point
, P
, px, py, pz
, hom, dehom
, invalidP
, origin
  -- * Line
, L
, lx, ly, lz
, invalidL, xAxis, yAxis
  -- * Infinite Objects
, far, farLine, close
  -- * Exported Modules
, module Data.Ratio
) where

import Data.Ratio
import ProjectiveGeometry.Types
import ProjectiveGeometry.QPlane.Internal
