module RP2.Types where

import Data.Complex

data RP2 = RP2 (Complex Double) (Complex Double) (Complex Double)

instance Eq RP2 where
  RP2 a b c == RP2 d e f = a*f == d*c && b*f == e*c

_i = 0 :+ 1

hom :: Double -> Double -> RP2
hom x y = RP2 (x :+ 0) (y :+ 0) 1

view :: RP2 -> (Complex Double, Complex Double, Complex Double)
view (RP2 a b c) = (a,b,c)

unview :: (Double, Double, Double) -> Maybe RP2
unview (0,0,0) = Nothing
unview (x,y,z) = Just (RP2 (x :+ 0) (y :+ 0) (z:+ 0))


pI, pJ :: RP2
pI = RP2 (-_i) 1 0
pJ = RP2 _i 1 0


det3 :: RP2 -> RP2 -> RP2 -> Complex Double
det3 (RP2 a d g) (RP2 b e h) (RP2 c f i) = a*e*i + b*f*g + c*d*h - g*e*c - h*f*a - i*d*b

dv :: RP2 -> RP2 -> RP2 -> RP2 -> RP2 -> Complex Double
dv x a b c d = (det3 a c x / det3 a d x) / (det3 b c x / det3 b d x)

cocircular :: RP2 -> RP2 -> RP2 -> RP2 -> Bool
cocircular a b c d = dv pI a b c d == dv pJ a b c d
