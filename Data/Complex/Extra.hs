module Data.Complex.Extra
  ( collinearOrigin
  , orthogonalOrigin
  , collinear3
  ) where

import Data.Complex

collinearOrigin, orthogonalOrigin
  :: (RealFloat a)
  => Complex a
  -> Complex a
  -> Bool
collinearOrigin x y = conjugate x * y == x * conjugate y

orthogonalOrigin x y = conjugate x * y == - x * conjugate y

collinear3
  :: (RealFloat a)
  => Complex a
  -> Complex a
  -> Complex a
  -> Bool
collinear3 a b c = collinearOrigin (a-c) (b-c)
