module CP1 where

import CP1.Types
import RP2.Types (RP2)
import qualified RP2.Types as RP2
import Data.Complex.Extra

minus :: CP1 -> CP1 -> Maybe CP1
minus a b =
  case (view a, view b) of
       ((x1,y1),(x2,y2)) -> unview (x1*y2 - x2*y1, y1*y2)

divide :: CP1 -> CP1 -> Maybe CP1
divide a b =
  case (view a, view b) of
       ((x1,y1),(x2,y2)) -> unview (x1*y2, y1*x2)

dv :: CP1 -> CP1 -> CP1 -> CP1 -> Maybe CP1
dv a b c d =
 do m_ac <- minus a c
    m_ad <- minus a d
    m_bc <- minus b c
    m_bd <- minus b d
    v1 <- divide m_ac m_ad
    v2 <- divide m_bc m_bd
    divide v1 v2

dv' a b c d = dv (hom a) (hom b) (hom c) (hom d) >>= unhom

liftT :: T -> Complex Double -> Maybe (Complex Double)
liftT t = unhom . applyT t . hom


toRP2 :: CP1 -> RP2
toRP2 p =
  case view p of
       (a :+ b,c :+ d) ->
        case RP2.unview (a*c + b*d, b*c - a*d, c*c + d*d) of
             Just res -> res
             Nothing -> error "absurd"

cocircular :: CP1 -> CP1 -> CP1 -> CP1 -> Bool
cocircular a b c d = RP2.cocircular (toRP2 a) (toRP2 b) (toRP2 c) (toRP2 d) 
